//config file

import { BuildOptions } from 'esbuild'
import path from 'path'
import { ClearPlugin } from './plugin/CleanPlugin'
import { HTMLPlugin } from './plugin/HTMLPlugin'

const mode = process.env.MODE || 'development'

const isDev = mode === 'development'
const isProd = mode === 'production'

function resolveRoot(...segments: string[]) {
  return path.resolve(__dirname, '..', '..', ...segments)
}
const config: BuildOptions = {
  outdir: resolveRoot('build'), //path to build directory
  entryPoints: [resolveRoot('src', 'index.jsx')], // entry point file path
  entryNames: '[dir]/bundle.[name]-[hash]', // to name of bundle, index.js by default
  allowOverwrite: true,
  bundle: true, //to clear unused code + import/export
  tsconfig: resolveRoot('tsconfig.json'),
  minify: isProd, //to minify code when is production mode
  sourcemap: isDev, // for debuging big files
  metafile: true, //for dynafic file add to index.html
  loader: {
    '.png': 'file',
    '.svg': 'file',
    '.jpg': 'file',
  },
  plugins: [ClearPlugin, HTMLPlugin({ //creates html in ./build
    title: 'Test Page title'
  })],
  // watch: isDev && {
  //   onRebuild(err, res) {
  //     if (err) {
  //       console.log(err)
  //     } else {
  //       console.log('build...')
  //     }
  //   },
  // },
}

export default config

//node .\config\build\esbuild-config.js
