import { Plugin } from 'esbuild'
import { rm } from 'fs/promises'

export const ClearPlugin: Plugin = {
  name: 'ClearPlugin',
  setup(build) {
    build.onStart(async () => {
      try {
        const outdir = build.initialOptions.outdir
        if (outdir) {
          await rm(outdir, { recursive: true })
        }
      } catch (error) {
        console.log('cant clear build directory')
      }
    })
    // build.onEnd(() => {
    //   console.log('Build end')
    // })
  },
}
