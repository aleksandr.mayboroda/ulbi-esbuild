import { Plugin } from 'esbuild'
import { rm, writeFile } from 'fs/promises'
import path from 'path'

interface HTMLPluginOptions {
  template?: string
  title?: string
  jsPath?: string[]
  cssPath?: string[]
}

const renderHtml = (options: HTMLPluginOptions): string => {
  return (
    options.template ||
    `<!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>${options.title}</title>
        ${options?.cssPath
          ?.map((path) => `<link href="${path}" rel="stylesheet" />`)
          .join('\n')}
      </head>
      <body>
        <div id="root"></div>
        ${options?.jsPath
          ?.map((path) => `<script src="${path}"></script>`)
          .join('\n')}
          <script>
            const evtSource = new EventSource('http://localhost:3000/subscribe')
            evtSource.onopen = function () { console.log('open') }
            evtSource.onerror = function () { console.log('error') }
            evtSource.onmessage = function () { 
                console.log('message')
                window.location.reload();
            }
          </script>
      </body>
    </html>`
  )
}

const preparePaths = (outputs: string[]) => {
  return outputs.reduce<Array<string[]>>(
    (acc, path) => {
      const [js, css] = acc
      const splitedFileName = path.split('/').pop() //take last part of path
      if (splitedFileName?.endsWith('.js')) {
        js.push(splitedFileName)
      } else if (splitedFileName?.endsWith('.css')) {
        css.push(splitedFileName)
      }
      return acc
    },
    [[], []]
  )
}

export const HTMLPlugin = (options: HTMLPluginOptions): Plugin => {
  return {
    name: 'HTMLPlugin',
    setup(build) {
      const outdir = build.initialOptions.outdir

      // build.onStart(async () => {
      //   try {
      //     if (outdir) {
      //       await rm(outdir, { recursive: true })
      //     }
      //   } catch (error) {
      //     console.log('cant clear build directory')
      //   }
      // })
      build.onEnd(async (result) => {
        const outputs = result?.metafile?.outputs
        const [jsPath, cssPath] = preparePaths(Object.keys(outputs || {})) //2 arrays: js & css

        console.log('outputs', jsPath, cssPath)

        if (outdir) {
          await writeFile(
            path.resolve(outdir, 'index.html'),
            renderHtml({ jsPath, cssPath, ...options })
          )
        }
      })
    },
  }
}
