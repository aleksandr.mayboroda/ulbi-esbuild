import React, { useState } from 'react'
import './second.css'

//@ts-ignore
import logo from './logo.png'

const App = () => {
  const [counter, setCounter] = useState<number>(4)

  const setError = () => {
    throw new Error('Hello, error!')
  }

  return (
    <div>
      <h1>value = {counter}</h1>
      <button onClick={() => setCounter((prev) => prev + 1)}>+ 1</button>
      <br />
      <button onClick={setError}>Set error</button>
      <div>
        <img src={logo} alt="logo" />
      </div>
    </div>
  )
}

export default App
