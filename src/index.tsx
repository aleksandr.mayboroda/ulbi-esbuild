import React from 'react'
import App from './App'
import ReactDOM from 'react-dom/client'

import './index.css'

const rootElement = document.getElementById('root')
if(!rootElement)
{
  throw new Error('root element is not found')
}

const root = ReactDOM.createRoot(rootElement)

root.render(<App />)
